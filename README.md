# DEPENDENCIES 
<br>

#### Server 
<br>

    npm install --save express //Express


#### Layouts 
<br>

    npm install --save express-handlebars //HandleBars

#### Database Handler 
<br>

    npm install --save mongoose //Mongo Handler

#### Body-Parser
<br>

    npm install body-parser

#### Connect-Flash 
<br>

    npm install --save connect-flash

#### Express-Sessions 
<br>

    npm install --save express-sessions

### Bcrypt
<br>

    npm install --save bcryptjs //Hashe senhas

#### Passport 
<br>

    npm install --save passport passport-local //Autenticacao de login com Local db