//Autenticacao pelo passport
const localStrategy = require('passport-local').Strategy

//DB
const mongoose = require('mongoose')

//Dehashing the password
const bcrypt = require('bcryptjs')


//Models
require('../models/Usuario')
const Usuario = mongoose.model('usuarios')

module.exports = function(passport){

    //usernameField = Qual o campo que voce quer analisar
    passport.use(new localStrategy({usernameField: 'email', passwordField: 'senha'}, (email, senha, done) => {
         
        //Pegando o email do banco de dados
        Usuario.findOne({email: email}).then((usuario) => {
            //Se a conta nao existir    
            if(!usuario){
                return done(null, false, {message: 'Esta conta nao existe'})
            }

            //Comparando a senha hasheada com a senha inserida
            bcrypt.compare(senha, usuario.senha, (error, batem) => {

                //Se as senhas baterem
                if(batem){
                    return done(null, usuario)
                }
                else{
                    return done(null, false, {message: 'Senha incorreta'})

                }
            })

        })

    }))

    //Salvando os dados do usuario em uma sessao
    passport.serializeUser((usuario, done) => {

        done(null, usuario.id)

    })

    passport.deserializeUser((id, done) => {
        //Achando o usurio pelo id
        Usuario.findById(id, (error, usuario) => {
            done(error, usuario)
        })
    })

}

