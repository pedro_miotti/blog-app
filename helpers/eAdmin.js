//Helper para apenas usuarios logados e administradores entrem na rota /admin
module.exports = {

    eAdmin: function(req, res, next){

        //Checando se o usuario esta logado && checando se ele e admin
        if(req.isAuthenticated() && req.user.eAdmin == 1){ 
            return next()
        }

        req.flash('error_msg', 'Voce tem que ser Admin para entar aqui !')
        res.redirect('/')

    }
}