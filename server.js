//Imports
const express = require('express')
const app = express()

const handlebars = require('express-handlebars') //Views

const bodyParser = require('body-parser') // Manipular Fomularios

const path = require('path') // Modulo para manipular pastas

const session = require('express-session')

const flash = require('connect-flash') // Tipo de sessao que so aparece uma vez (Bom para mensagens de erro e sucesso)

const mongoose = require('mongoose') // DB

const passport = require('passport') //Autenticacao de login
require('./config/auth')(passport) // Arquivo de configuracao do passport

//Importing Routes
const admin = require('./routes/admin')

const usuario = require('./routes/usuario')


//Models
require('./models/Post')
const Post = mongoose.model('postagens')

require('./models/Categoria')
const Categoria = mongoose.model('categorias')

// OBS: Tudo que tem app.use e para configuracao de middlewares

//CONFIG
    // Sessions
        app.use(session({
            secret: "08%10$99&",  //Chave para gerar uma sessao
            resave: true,
            saveUninitialized: true
        }))

    // Passport
        app.use(passport.initialize()) // Inicializando 
        app.use(passport.session()) // Configurando para salvar na sessao

    // Flash
        app.use(flash())

    // Middleware
        app.use((req, res, next) => {
            // configurando variaveis globais
            res.locals.success_msg = req.flash("success_msg") // Success message 
            res.locals.error_msg = req.flash("error_msg") // Error message
            res.locals.error = req.flash('error')
            res.locals.user = req.user || null // .user armazena dados do usuario logado
            next()
        })  

    // Body-Paser
        app.use(bodyParser.json())
        app.use(bodyParser.urlencoded({ extended: true }))

    // Handlebars
        app.engine('handlebars', handlebars({defaultLayout: 'main'}))
        app.set('view engine', 'handlebars')

    // Mongoose
        mongoose.Promise = global.Promise
        //Conectando ao Database
        mongoose.connect("mongodb://localhost/blogapp", { useNewUrlParser: true , useUnifiedTopology: true} ).then(() => {
            console.log('Database OK !')
        }).catch((err) => {
            console.log("Erro ao se conectar ao Database : " + err)
        })

    // Pasta Public
        app.use(express.static(path.join(__dirname, 'public')))
        
    


//ROTAS 

    
    // ADMIN
    app.use('/admin', admin)

    app.get('/', (req, res) => {
        Post.find().populate('categoria').sort({data: 'desc'}).then((postagens) => {
            res.render('index', {postagens: postagens})
        }).catch((err) => {
            req.flash('error_msg', 'Erro interno !  ' + err)
            res.redirect('/404')
        })
        
    })

    //Pagina de 404
    app.get('/404', (req, res) => {
        res.send("Error 404")
    })

    //
    app.get('/postagem/:slug', (req, res) => {

        Post.findOne({slug: req.params.slug}).then((postagem) => {
            if(postagem){
                res.render("postagem/index", {postagem: postagem})
            }

            else{

                req.flash('error_msg', 'Esta postagem nao existe !  ' + err)
                res.redirect('/')

            }

        }).catch((err ) => {
            req.flash('error_msg', 'Erro interno !  ' + err)
            res.redirect('/')
        })

    })

    //Rota para procurar as categorias
    app.get('/categorias', (req, res) => {
        Categoria.find().then((categorias) => {
            res.render('categorias/index', {categorias: categorias})

        }).catch((err) => {
            req.flash('error_msg', 'Erro interno ! ' + err)
            res.redirect("/")
        })
    })

    //Procurando as os posts daquela categoria pelo slug quando clica no link da categoria
    app.get('/categorias/:slug', (req, res) => {
        Categoria.findOne({slug: req.params.slug}).then((categoria) => {
            if(categoria){

                Post.find({categoria: categoria._id}).then((postagen) => {

                    res.render('categorias/postagens', {postagen: postagen, categoria: categoria})

                }).catch((err) => {
                    req.flash('error_msg', 'Erro ao carregar os posts ! ' + err)
                    res.redirect("/")
                })
            }

            else{
                req.flash('error_msg', 'Esta categoria nao existe ! ' + err)
                res.redirect("/")

            }

        }).catch((err) => {
            req.flash('error_msg', 'Erro interno ao carregar a pagina da categoria ! ' + err)
            res.redirect("/")
        })
    })

    // Usuarios
    app.use("/usuarios", usuario)






//CONFIGURANDO PORTA
const PORT = 8081
app.listen(PORT, () => {
    console.log("Servidor OK !")
})