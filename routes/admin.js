//Imports
const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')

// Helpers
const {eAdmin} = require('../helpers/eAdmin') // {funcao} = pega uma funcao especifica do arquivo

// Models
require('../models/Categoria')
const Categoria = mongoose.model('categorias')

require('../models/Post')
const Post = mongoose.model('postagens')





//Rotas
    router.get('/', eAdmin,  (req, res) => {
        res.render("admin/index")
    })


    //Rota Categoria
    router.get('/categoria', eAdmin, (req, res) => {
        // Colocando as categorias que estao no DB na view categoria
        Categoria.find().sort({date: 'desc'}).then((categorias) => {

            res.render('admin/categorias', {categorias: categorias})

        }).catch(() => {

            req.flash('error_msg', 'Erro ao listar as categorias, tente novamente !')
            res.redirect('/admin')

        })
    })

    router.get('/categoria/add', eAdmin, (req, res) => {
        res.render('admin/addcategorias')
    })

    //Postando nova categoria
    router.post('/categoria/nova', eAdmin, (req, res) => {
        // Validacao de formulario
        let erros = []

        // Configurando erros
            // Validcacao NOME
            if(!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null){
                erros.push({texto: " *Nome invalido "})
            }
            // Validcacao SLUG
            if(!req.body.slug || typeof req.body.slug == undefined || req.body.slug == null){
                erros.push({texto: "*Slug invalido "})
            }

            if(req.body.nome.length < 1){
                erros.push({texto: "*Nome da categoria muito pequena"})
            }
        
        // Verificando se ha erros
        if(erros.length > 0){
            res.render("admin/addcategorias", {erros: erros})

        }else{

            // Adicionando a categoria ao BD
            const novaCategoria = {
                nome: req.body.nome,
                slug: req.body.slug
            }

            new Categoria(novaCategoria).save().then(() => {
                console.log('Categoria salva com sucesso !')
                req.flash('success_msg', 'Categoria criada com sucesso !')
                res.redirect("/admin/categoria") // Se nao tiver erros salvar ao BD e redirecionar a /admin/categorias
            }).catch((err) =>{
                req.flash('error_msg', 'Erro ao salvar a categoria, tente novamente !' )
                console.log('Erro ao salvar categoria ! :' + err)
            })
                
        }
    })

    //Editar categorias -- Pegando o ID para editar
    router.get('/categoria/edit/:id', eAdmin, (req, res) =>{
        //Para prencher os placeholders
        Categoria.findOne({_id: req.params.id}).then((categoria) => {
            res.render('admin/editcategorias', {categoria: categoria})
        }).catch((err) => {
            req.flash('error_msg', 'Essa categoria nao existe !')
            res.redirect('/admin/categoria')
        })
        
    })

    // Salvando no BD a edicao 
    router.post('/categoria/edit', eAdmin, (req, res) => {

        Categoria.findOne({_id: req.body.id}).then((categoria) => {

            categoria.nome = req.body.nome
            categoria.slug = req.body.slug

            categoria.save().then(() => {

                req.flash('success_msg', 'Categoria salva com sucesso !')
                res.redirect('/admin/categoria')

            }).catch((err) => {
                req.flash('error_msg', 'Erro ao salvar a edicao da categoria !  ' + err)
                res.redirect('/admin/categoria')
            })

        }).catch((err) => {
            req.flash('error_msg', 'Erro ao editar categoria !  ' + err)
            res.redirect('/admin/categoria')
        })
    })

    // Deletando Categorias
    router.post('/categoria/deletar', eAdmin, (req, res) => {
        Categoria.deleteOne({_id: req.body.id}).then(() => {

            req.flash('success_msg', 'Categoria deletada com sucesso !')
            res.redirect('/admin/categoria')

        }).catch((err) => {

            req.flash('error_msg', 'Erro ao deletar categoria !  ' + err)
            res.redirect('/admin/categoria')

        })
    })

    // Listagem de postagens
    router.get('/postagens', eAdmin, (req, res) => {
        Post.find().populate('categoria').sort({data: 'desc'}).then((postagens) => {
            res.render('admin/postagens', {postagens: postagens})
        }).catch((err) => {
            req.flash('error_msg', 'Erro ao carregar as postagens !  ' + err)
            res.redirect('/admin')
        })
        
    })
    
    //Add Posts
    router.get('/postagens/add', eAdmin, (req, res) => {
        Categoria.find().sort({date: 'desc'}).then((categorias) => {
            res.render('admin/addpostagem', {categorias: categorias})
        }).catch((err) => {
            req.flash('error_msg', 'Erro ao carregar o formulario !  ' + err)
            res.redirect('/admin')
        })
    })


    router.post('/postagens/nova', eAdmin, (req, res) => {
        //Validacao de formulario
        let erros = []

        if(req.body.categoria == '0'){
            erros.push({texto: "Categoria Invalida, resistre uma categoria !"})
        }

        if(erros.length > 0){
            res.render('admin/addpostagem', {erros: erros})
        }
        else{
            //Adcionando postagem no DB
            const novaPostagem = {
                titulo: req.body.titulo,
                slug: req.body.slug,
                descricao: req.body.descricao,
                conteudo: req.body.conteudo,
                categoria: req.body.categoria
            }

            new Post(novaPostagem).save().then(() => {

                req.flash('success_msg', 'Postagem criada com sucesso !')
                res.redirect('/admin/postagens')

            }).catch((err) => {

                req.flash('error_msg', 'Erro ao salvar a postagem !  ' + err)
                res.redirect('/admin/postagens')

            })
        }
    })


    //Editando Posts
    router.get('/postagens/edit/:id', eAdmin, (req, res) => {
        //Aqui estamos usando o req.params.id porque estamos pegando do banco de dados
        Post.findOne({_id: req.params.id}).then((postagem) => {

            Categoria.find().then((categorias) => {

                res.render('admin/editpostagens', {categorias: categorias, postagem: postagem})

            }).catch((err) => {

                req.flash('error_msg', 'Erro ao carregar as categorias !  ' + err)
                res.redirect('/admin/postagens')

            })

        }).catch((err) => {

            req.flash('error_msg', 'Erro ao carregar o formulario de edicao !  ' + err)
            res.redirect('/admin/postagens')

        })
    })


    //Atualizando as edicoes no DB
    router.post('/postagens/edit', eAdmin, (req, res) => {
        //Aqui estamos usando o req.body.id porque estamos pegando do formulario editpostagens.handlebars
        Post.findOne({_id: req.body.id}).then((postagem) => {

            postagem.titulo = req.body.titulo
            postagem.slug = req.body.slug
            postagem.descricao = req.body.descricao
            postagem.conteudo = req.body.conteudo
            postagem.categoria = req.body.categoria

            postagem.save().then(() => {

                req.flash('success_msg', 'Postagem editada com sucesso !')
                res.redirect('/admin/postagens')

            }).catch((err) => {

                req.flash('error_msg', 'Erro ao salvar a edicao !  ' + err)
                res.redirect('/admin/postagens')

            })

        }).catch((err) => {

            req.flash('error_msg', 'Erro ao salvar a edicao !  ' + err)
            res.redirect('/admin/postagens')

        })
    })


    //Deletando Post
    router.post('/postagens/deletar', eAdmin, (req, res) => {

        Post.deleteOne({_id: req.body.id}).then(() => {

            req.flash('success_msg', 'Postagem deletada com sucesso !')
            res.redirect('/admin/postagens')

        }).catch(() => {

            req.flash('error_msg', 'Erro ao deletar a postagem !  ' + err)
            res.redirect('/admin/postagens')

        })

    })












//Exporting router
module.exports = router


































