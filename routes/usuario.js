//Imports
const express = require('express')
const router = express.Router()

const mongoose = require('mongoose')

const bcrypt = require('bcryptjs')

const passport = require('passport')


//Models
require('../models/Usuario')
const Usuario = mongoose.model('usuarios')



//Rotas
    // Registro de usuario 
    router.get('/registro', (req, res) => {
        res.render('usuarios/registro')
        
    })

    router.post('/registro', (req, res) => {
        // Validacao do formulario de criacao de usuario
        let erros = []

        if(!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null){
            erros.push({texto: " * Nome invalido "})
        }
        // Validcacao Email
        if(!req.body.email || typeof req.body.email == undefined || req.body.email == null){
            erros.push({texto: "* Email invalido "})
        }
        // Validacao senha 
        if(!req.body.senha || typeof req.body.senha == undefined || req.body.senha == null){
            erros.push({texto: "* Email invalido "})
        }
        // // Validacao tamanho da senha 
        if(req.body.senha.length < 4){
            erros.push({texto: "* Senha muito curta"})
        }
        // Validacao se a senha1 e a senha2 sao iguais
        if(req.body.senha != req.body.senha2){
            erros.push({texto: "* Senha diferentes, tente novamente"})
        }

        if(erros.length > 0){
            //Colocando os erros na tela
            res.render('usuarios/registro', {erros: erros})

        }
        else{
            //Validando se ja existe um email no banco de dados
            Usuario.findOne({ email: req.body.email }).then((usuario) => {
                if(usuario){
                    req.flash('error_msg', 'Esse E-mail ja esta em uso ! ')
                    req.redirect('/registro')
                }
                else{

                    //Adicionando as informacoes do formulario em uma variavel
                    const novoUsuario = new Usuario({
                        nome: req.body.nome,
                        email: req.body.email,
                        senha: req.body.senha
                    })

                    //Hashing a senha -- Salt e para acrescentar mais caracteres para a senha hasheada
                    bcrypt.genSalt(10, (error, salt) => {
                        bcrypt.hash(novoUsuario.senha, salt, (error, hash) => {
                            if(error){
                                req.flash('error_msg', 'Houve um erro ao salvar o usuario !')
                                res.redirect('/')
                            }
                            

                            //Defindo a senha do usuario como hasheada
                            novoUsuario.senha = hash

                            //Salvando usuario no DB
                            novoUsuario.save().then(() => {
                                req.flash('success_msg', 'Usuario criado com successo !')
                                res.redirect('/')
                            }).catch((err) => {
                                req.flash('error_msg', ('Erro ao salvar o usuario') + err)
                            })

                            
                        })
                    }) 

                }

            }).catch((err) => {
                req.flash('error_msg', 'Houve um erro interno ! ' + err)
                res.redirect('/')
            })
        }
    
    })

//Tela de login
router.get('/login', (req, res) => {
    res.render('usuarios/login')
})

// Rota de autenticacao de usuario
router.post('/login',(req, res, next) => {

    //Funcao para fazer a autenticacao
    passport.authenticate('local' ,{

        //Caminho caso a autenticacao seja com successo
        successRedirect: "/",

        //Caminho caso a autenticacao falhe
        failureRedirect: "/usuarios/login",
        failureFlash: true

    })(req, res, next)

})

// Rota de Logout
router.get('/logout', (req, res) => {

    req.logout()
    req.flash('success_msg', 'Deslogado com sucesso !')
    
    res.redirect('/')



})

module.exports = router























